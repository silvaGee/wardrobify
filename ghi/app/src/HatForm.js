import React, { useEffect, useState } from 'react';
function HatForm () {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        style_name: "",
        fabric: "",
        color: "",
        picture_url: "",
        location: "",
    })

    const fetchData = async () => {
        const locationUrl = "http://localhost:8100/api/locations/";
        try{
            const response = await fetch(locationUrl);
            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        } catch(e) {
            console.log(e)
        }

    }
    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                style_name: "",
                fabric: "",
                color: "",
                picture_url: "",
                location: "",

            });
        }
    }
    //we're going to replate multiple form change eentListener functions with just this one below
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            //when we do the three dots we are copying over the previous form data into our new state object
            ...formData,
            [inputName]: value //adding currently engaged input key and value
        });

    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                {/* <!-- Now, each field in our form references the same function --> */}
                <input onChange={handleFormChange} value={formData.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.href} value={location.href}>{location.closet_name}</option>
                      //key is used her because we used map which is an array.  We have to use key so React can keep track of every single thing;
                      //anytime you use map, you ALWAYS need to have a key THAT IS A UNIQUE IDENTIFIER, so use href or id; value is the piece of data we actually use in line 53
                      //the last {location.href} tag is what the user sees, so instead of href there, you can put whatever you want the user to see
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default HatForm;
