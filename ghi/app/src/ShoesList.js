import { useState, useEffect } from "react";

function ShoesList() {
  const [shoes, setShoes] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/shoes/");
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };
  const deleteShoes = async function (shoesToDelete) {
    console.log(shoesToDelete);
    const response = await fetch(
      `http://localhost:8080/api/shoes/${shoesToDelete}/`,
      {
        method: "DELETE",
      }
    );
    if (response.ok) {
      getData();
    } else {
      console.log(response);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Delete</th>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoe) => {
          console.log(shoe);
          return (
            <tr key={shoe.id}>
              <td>
                <div>
                  <button onClick={() => deleteShoes(shoe.id)}>Delete</button>
                </div>
              </td>
              <td>{shoe.model_name}</td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.color}</td>
              <td>
                <div>
                  <img src={shoe.picture_url} className="card-img-top" />
                </div>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
